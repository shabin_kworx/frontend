import { Component, OnInit } from '@angular/core';
import { TodoDataService } from './todo-data.service';
import { Todo } from './todo';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [TodoDataService]
})
export class AppComponent implements OnInit {

  todos: Todo[] = [];

  constructor(
    private todoDataService: TodoDataService
  ) {
  }

  public ngOnInit() {
    // this.todoDataService
    //   .getAllTodos()
    //   .subscribe(
    //     (todos) => {
    //       this.todos = todos;
    //     }
    //   );
  }

  onAddTodo(todo) {
    todo.id = Date.now();
    let newTodo = new Todo(todo);
    this.todos = this.todos.concat(newTodo);
    // this.todoDataService
    //   .addTodo(todo)
    //   .subscribe(
    //     (newTodo) => {
    //       this.todos = this.todos.concat(newTodo);
    //     }
    //   );
  }

  onToggleTodoComplete(todo) {

    todo.complete = true;
    /* this.todoDataService
      .toggleTodoComplete(todo)
      .subscribe(
        (updatedTodo) => {
          todo = updatedTodo;
        }
      ); */
  }

  onRemoveTodo(todo) {
    this.todos = this.todos.filter((t) => t.id !== todo.id);
    // this.todoDataService
    //   .deleteTodoById(todo.id)
    //   .subscribe(
    //     (_) => {
    //       this.todos = this.todos.filter((t) => t.id !== todo.id);
    //     }
    //   );
  }
}
